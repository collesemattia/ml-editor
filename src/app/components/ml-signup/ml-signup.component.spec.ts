import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MlSignupComponent } from './ml-signup.component';

describe('MlSignupComponent', () => {
  let component: MlSignupComponent;
  let fixture: ComponentFixture<MlSignupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MlSignupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlSignupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
