import { Component, OnInit } from '@angular/core';
import { IConfiguration, ILabeledText } from 'src/app/interfaces/interfaces';
import { ClassifierService, configDefault } from '../../services/classifier.service';
import { MatSnackBar } from '@angular/material';
import { ShowProgressSpinnerService } from '../../services/show-progress-spinner.service';

@Component({
  selector: 'app-ml-configuration',
  templateUrl: './ml-configuration.component.html',
  styleUrls: ['./ml-configuration.component.css']
})
export class MlConfigurationComponent implements OnInit {

  config: IConfiguration;
  traindatum: ILabeledText;

  jsonCopy(src) {
    return JSON.parse(JSON.stringify(src));
  }

  constructor(
    private classifierService: ClassifierService,
    private snackBar: MatSnackBar,
    private progressSpinner: ShowProgressSpinnerService
  ) {
    this.config = this.jsonCopy(configDefault);
  }

  getState() {
    return this.classifierService.getState();
  }

  ngOnInit() {
  }

  reset() {
    this.config = this.jsonCopy(configDefault);

    this.classifierService.setConfiguration(this.config);
  }

}
