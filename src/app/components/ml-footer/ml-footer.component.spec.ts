import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MlFooterComponent } from './ml-footer.component';

describe('MlFooterComponent', () => {
  let component: MlFooterComponent;
  let fixture: ComponentFixture<MlFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MlFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MlFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
