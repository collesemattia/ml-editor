import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ClassifierService } from '../../services/classifier.service';
import { IRunResult, MLState } from '../../interfaces/interfaces';
import { LabeledDataManagerService } from 'src/app/services/labeled-data-manager.service';
import { ImageClassifierService } from 'src/app/services/image-classifier.service';
import { TranslateService } from '@ngx-translate/core';
import { MlWebCamComponent } from '../ml-web-cam/ml-web-cam.component';
import { ScratchManagerService } from 'src/app/services/scratch-manager.service';

@Component({
  selector: 'app-ml-test-model',
  templateUrl: './ml-test-model.component.html',
  styleUrls: ['./ml-test-model.component.css']
})
export class MlTestModelComponent implements OnInit {

  testText: string;
  result: IRunResult;
  resultImage: {};
  prediction;
  mostLikelyClass: string;
  testImageURL: string = null;
  confidenceText: string;
  @ViewChild('fileImagesElement', { static: true }) fileElement: ElementRef;
  @ViewChild('webcam', { static: false }) webcam: MlWebCamComponent;

  constructor(
    public labeledDataManager: LabeledDataManagerService,
    private imageClassifierService: ImageClassifierService,
    private classifierService: ClassifierService,
    private translate: TranslateService,
    private scratchManager: ScratchManagerService,
    private snackBar: MatSnackBar) { }

  ngOnInit() {

  }

  getState() {
    return this.labeledDataManager.state;
  }

  testImage() {
    if(!this.checkIfTestCanBeDone()) return;
      this.fileElement.nativeElement.click();
  }

  testImageFromWebcam() {
    if(!this.checkIfTestCanBeDone()) return;
    this.webcam.setCardClass('cam-card-test');
    this.webcam.enableVideo();
  }

  takeSnapshot(b64Img) {
    let image = document.createElement('img');

    this.testImageURL = b64Img;
    image.src = b64Img;

    image.onload = () => {
      this.imageClassifierService.classify(image)
        .then(p => {
          // console.log(p);
          this.resultImage = p;
          this.confidenceLevel(p[0][1], p[0][0]);
        });
    }
  }

  onLoaded(e) {
    // console.log("aqui");
    let files = e.target.files;

    // console.log(files);

    for (let file of files) {
      let fileReader = new FileReader();
      fileReader.readAsDataURL(file);

      fileReader.onloadend = () => {
        this.testImageURL = fileReader.result.toString();
        let image = document.createElement('img');

        image.src = fileReader.result.toString();

        image.onload = () => {
          this.imageClassifierService.classify(image)
            .then(p => {
              // console.log(p);
              this.resultImage = p;
              this.confidenceLevel(p[0][1], p[0][0]);
            });
        }
      }
    }
  }

  confidenceLevel(confidence, label) {
    let textLabel = "";
    this.mostLikelyClass = label
    if (confidence < 0.50) {
      textLabel = 'confidence_text.very_low';
    } else if (confidence >= 0.50 && confidence < 0.7) {
      textLabel = 'confidence_text.low';
    } else if (confidence >= 0.7 && confidence < 0.8) {
      textLabel = 'confidence_text.normal';
    } else if (confidence >= 0.8 && confidence < 0.9) {
      textLabel = 'confidence_text.high';
    } else if (confidence >= 0.9) {
      textLabel = 'confidence_text.very_high';
    } else {
    }

    this.translate.get(textLabel, { label: label }).subscribe((res: string) => {
      this.confidenceText = res;

    });
  }

  checkIfTestCanBeDone() {
    if (this.labeledDataManager.state == MLState.EMPTY || this.labeledDataManager.state == MLState.UNTRAINED) {
      this.snackBar.open("Antes hay que recopilar datos y aprender a partir de ellos",
        'Crea un modelo', {
        duration: 3000,
        verticalPosition: 'top'
      });
      return false;
    }

    return true;
  }

  test() {
    if(!this.checkIfTestCanBeDone()) return;
    this.result = this.classifierService.run(this.testText);
    console.log(this.result);
    let sum = 0;
    let prediction = [];
    for (let p in this.result.prediction) {
      prediction.push([p, this.result.prediction[p]]);
    }

    prediction.sort((a, b) => {
      return b[1] - a[1];
    });

    this.prediction = prediction;
    this.confidenceLevel(prediction[0][1], prediction[0][0]);
    console.log(this.prediction);
  }

  loadScratch() {
    this.scratchManager.load();
  }

}