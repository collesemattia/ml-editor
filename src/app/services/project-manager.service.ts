import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { AuthenticationService } from './authentication.service';
import { ConfigService } from './config.service';
import { IProject } from '../interfaces/interfaces';

@Injectable({
  providedIn: 'root'
})
export class ProjectManagerService {

  projects = [];

  httpOptions = {}
  url = ConfigService.settings.api.url_base +
    ConfigService.settings.api.path;

  constructor(private httpClient: HttpClient,
    private authService: AuthenticationService) {
  }

  setHttpHeaders(){
    this.httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        'Authorization': 'Token ' + this.authService.getCurrentToken()
      })
    };
  }

  getProject(project_id) {
    this.setHttpHeaders();
    const url = this.url + project_id;
    return this.httpClient.get<IProject>(url, this.httpOptions);
  }

  getUserProjects() {
    this.setHttpHeaders();
    return this.httpClient.get<IProject[]>(this.url, this.httpOptions);
  }

  getSharedProjects() {
    this.setHttpHeaders();
    const url = this.url + '?shared=1';
    return this.httpClient.get<IProject[]>(url, this.httpOptions);
  }

  createProject(project) {
    this.setHttpHeaders();
    return this.httpClient.post<IProject>(this.url, project, this.httpOptions);
  }

  reinventProject(project) {
    this.setHttpHeaders();

    const url = this.url + 'operation/' + project.id + '/reinvent';

    this.httpClient.put(url, {}, this.httpOptions).subscribe();

    project.reinventions = 0;
    project.likes = 0;
    project.shared = 0;
    return this.createProject(project);
  }

  likeProject(project) {
    this.setHttpHeaders();

    const url = this.url + 'operation/' + project.id + '/like';

    return this.httpClient.put(url, {}, this.httpOptions);
  }

  deleteProject(id) {
    this.setHttpHeaders();
    const url = this.url + id
    return this.httpClient.delete<IProject>(url, this.httpOptions)
  }

  updateProject(id, data) {
    this.setHttpHeaders();
    const url = this.url + id;
    return this.httpClient.put<IProject>(url, data, this.httpOptions);
  }

  shareProject(project) {
    project.shared = 1;
    return this.updateProject(project.id, project)
  }

  unShareProject(project) {
    project.shared = 0;
    return this.updateProject(project.id, project)
  }

  isMine(project): Boolean{
    return this.authService.getCurrentUsername == project.username
  }

}
