import { Injectable } from '@angular/core';
import { MLState, IConfiguration, TText, Label, ILabeledData, ITrainResult, IRunResult, ILabeledText } from '../interfaces/interfaces';
import { Observable, of, from } from 'rxjs';
import { v4 as uuid } from 'uuid';
import * as BrainText from 'brain-text'
import { LabeledDataManagerService } from './labeled-data-manager.service';

export let configDefault: IConfiguration = {
  iterations: 3000, // the maximum times to iterate the training data
  errorThresh: 0.005, // the acceptable error percentage from training data
  log: true, // true to use console.log, when a function is supplied it is used
  logPeriod: 10, // iterations between logging out
  learningRate: 0.3, // multiply's against the input and the delta then adds to momentum
  momentum: 0.1, // multiply's against the specified "change" then adds to learning rate for change
};

@Injectable({
  providedIn: 'root'
})
export class ClassifierService {

  private id: string;
  private configuration: IConfiguration;
  private brainText;
  private traindata = [];
  public trainResult: ITrainResult;

  constructor(
    private labeledDataManager: LabeledDataManagerService) {
    this.configuration = configDefault;
    this.clear("es");
  }

  addEntry(entry) {
    let e = { label: entry.label, text: entry.data }
    console.log(e);
    this.brainText.addOneData(e);
  }

  removeEntry(entry) {
    let e = { label: entry.label, text: entry.data }
    this.brainText.removeData(e);
  }

  /** 
   * Train the model, update the state and set the localStorage "easyml_model"
   * in order to be shared with scratch
   */
  train(lang): Observable<any> {
    this.clear(lang);
    for (let label of this.labeledDataManager.labels) {
      for (let data of this.labeledDataManager.labelsWithData.get(label)) {
        this.addEntry({ label: label, data: data })
      }
      console.log(this.brainText._traindata);
    }
    return from(this.brainText.train().then((r) => {
      this.id = uuid();
      console.log('Updating model storage');
      let model = this.toJSON(MLState.TRAINED);
      localStorage.setItem("easyml_model", model);
      //this.storageService.set("easyml_model", model);
      this.labeledDataManager.state = MLState.TRAINED;
      return r;
    }));
  }

  /**
   * Run the model built after training
   * @param text 
   */
  run(text: TText): IRunResult {
    let r = this.brainText.run(text);

    return r;
  }

  clear(lang) {
    this.traindata = [];
    this.brainText = new BrainText(lang);
    console.log(this.brainText);
    this.brainText.setConfiguration(this.configuration);
  }

  getConfiguration(): IConfiguration {
    return this.configuration;
  }

  setConfiguration(config: IConfiguration) {
    this.configuration = config;
    this.brainText.setConfiguration(this.configuration);
  }

  getState() {
    return this.brainText.getState();
  }

  toJSON(state) {
    let m = {
      id: this.id,
      state: state,
      modelJSON: this.brainText.toJSON()
    }
    return JSON.stringify(m);
  }

  fromJSON(m) {
    let lang = localStorage.getItem("language");
    this.brainText = new BrainText(lang);
    console.log(this.brainText);
    this.brainText.setConfiguration(this.configuration);
    this.brainText.fromJSON(m);
  }
}
