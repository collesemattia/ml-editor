# LearningML

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 8.3.19.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `--prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Compilación

    # ng build --base-href /editor/ --prod=true

## Problema en el entorno de desarrollo para leer el modelo generado por el editor de LearningML desde Scratch 

LearningML usa tensorflow.js para construir los modelos de ML. Una vez construidos y entrenados se almacenan localmente en el navegador usando `indexeddb`. Posteriormente, desde Scratch se carga ese mismo modelo. El problema es que `indexeddb` solo puede compartirse entre aplicaciones que sirven desde el mismo dominio. Esto no es un problema en producción puesto que colocaremos a ambas aplicaciones en el mismo dominio, pero en desarrollo sí lo es, puesto que una aplicación se ejecuta en un puerto y la otra en otro puerto distinto, y esto es considerado por los browsers como dominios distintos. 

La solución, para salir del paso cuando necesite cargar el modelo desde Scratch es crear un reverse proxy con nginx, de manera que se pueda acceder tanto a Scratch como a LearningMl desde el mismo dominio.

La configuración de nginx para conseguir esto es (sección server):

    server {
        listen       8080;
        server_name  localhost;

        proxy_set_header Host $host;
        proxy_set_header X-Forwarded-For $remote_addr;

        location /learningml {
            proxy_pass http://localhost:4200/learningml;
        }
        location / {
            proxy_pass http://localhost:8601/;
        }
    }

Y para arrancar/parar el servicio

    # nginx
    # nginx -s stop

Una vez arrancado el servicio se accede a LearningML desde:

    http://localhost:8080/learningml

Y a Scratch desde:

    http://localhost:8080

Es importante para que esto funcione arrancar el servidor de desarrollo de learningML así:

    # ng serve --base-href /learningml/ --live-reload=false 

En MacOs, aunque se puede instalar nginx con brew, tuve un problema con la librería SSL, que provocaba que mysql dejase de funcionar con python. Así que lo he instalado construyendolo desde las fuentes (https://gist.github.com/beatfactor/a093e872824f770a2a0174345cacf171)
    
## Plan de pruebas

### Prueba 1

1. Cargamos un proyecto existente de imágenes (insectos)
2. Entrenamos desde LML
3. Abrimos Scratch desde LML
4. Abrimos el proyecto `test-lml-images.sb3`
5. Pulsamos tecla v para activar webcam
6. Pulsamos varias veces la tecla t para añadir imágenes al dataset
7. Comprobar que en LML se ha creado la clase "juanda" y tiene las imágenes
8. Entrenamos desde LML
9. Nos vamos a Scratch y probamos que la imagen de la webcam la clasifica correctamente en la nueva clase que se ha creado.

### Prueba 2

1. Cargamos un proyecto existente de imágenes (insectos)
2. Entrenamos desde LML
3. Abrimos Scratch desde LML
4. Abrimos el proyecto `test-lml-images.sb3`
5. Pulsamos tecla v para activar webcam
6. Pulsamos varias veces la tecla t para añadir imágenes al dataset
7. Comprobar que en LML se ha creado la clase "juanda" y tiene las imágenes
8. En Scratch pulsar tecla l para aprendizaje y construcción del nuevo modelo
9. Comprobar que clasifica bien la imagen de la cámara asociandola a la clase que se acaba de crear.
10. Comprobar en LML->evaluación que se clasifica bien la imágen de la webcam

### Prueba 3

1. Cargamos un proyecto existente de imágenes (insectos)
2. Entrenamos desde LML
3. Abrimos Scratch desde LML
4. Abrimos el proyecto `test-lml-images.sb3
5. Volvemos a LML y añadimos una nueva clase con imágenes de la webcam
6. Entrenamos desde LML
7. Nos vamos a Scratch y encendemos video con la tecla v
8. Usamos la tecla x para clasificar el video y vemos que lo hace bien.


### GPL License

LeaningML, the easiest way to learn Machine Learning fundamentals

Copyright (C) 2020 Juan David Rodríguez García & KGBLIII

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as
published by the Free Software Foundation, either version 3 of the
License, or (at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU Affero General Public License for more details.

You should have received a copy of the GNU Affero General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.